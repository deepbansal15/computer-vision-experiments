#ifndef __ALIGN_IMAGES_H__
#define __ALIGN_IMAGES_H__

namespace cv
{
    class Mat;
}

namespace cv_exp
{
    namespace align_images
    {
        static int MAX_FEATURES = 400;
        static float KEEP_PERCENTAGE = 0.30f;
        void align(cv::Mat& ref_img,cv::Mat& misaligned_img);
    }
}

#endif