#include <opencv2/opencv.hpp>
#include <opencv2/features2d.hpp>
#include <opencv2/xfeatures2d.hpp>

#include "align_images.h"

namespace cv_exp
{
    namespace align_images
    {
        void align(cv::Mat& ref_img,cv::Mat& misaligned_img)
        {
            // Convert images to gray scale
            cv::Mat ref_image_grey;
            cv::cvtColor(ref_img,ref_image_grey,CV_BGR2GRAY);

            cv::Mat misaligned_image_grey;
            cv::cvtColor(misaligned_img,misaligned_image_grey,CV_BGR2GRAY);

            // Create ORB feature detector
            cv::Ptr<cv::Feature2D> orb = cv::ORB::create(MAX_FEATURES);

            // Compute keypoints for reference image
            std::vector<cv::KeyPoint> ref_keypoints;
            cv::Mat ref_descriptors;
            orb->detectAndCompute(ref_image_grey,cv::Mat(),ref_keypoints,ref_descriptors);

            // Compute keypints for Misaligned image
            std::vector<cv::KeyPoint> misaligned_keypoints;
            cv::Mat misaligned_descriptors;
            orb->detectAndCompute(misaligned_image_grey,cv::Mat(),misaligned_keypoints,misaligned_descriptors);

            // Create descriptor matcher
            cv::Ptr<cv::DescriptorMatcher> matcher = 
                        cv::DescriptorMatcher::create(cv::DescriptorMatcher::BRUTEFORCE_HAMMING);

            // Match descriptors from reference and misaligned
            std::vector<cv::DMatch> matches;
            matcher->match(misaligned_descriptors,ref_descriptors,matches);
            
            // Sort matches
            std::sort(matches.begin(),matches.end());

            //cv::Mat matches_img1;
            //cv::drawMatches(ref_img,ref_keypoints,misaligned_img,misaligned_keypoints,matches,matches_img1);
            //cv::imwrite("matches_all.jpg", matches_img1);

            // Drop matches
            int keep_count = matches.size() * KEEP_PERCENTAGE;
            matches.erase(matches.begin() + keep_count,matches.end());
            
            cv::Mat matches_img2;
            cv::drawMatches(ref_img,ref_keypoints,misaligned_img,misaligned_keypoints,matches,matches_img2);
            cv::imwrite("../res/matches_top.jpg", matches_img2);

            // Get image points
            std::vector<cv::Point2f> ref_points,misaligned_points;
            int matches_count = matches.size();
            for(int i=0;i<matches_count;i++)
            {   
                ref_points.push_back(ref_keypoints[matches[i].trainIdx].pt);
                misaligned_points.push_back(misaligned_keypoints[matches[i].queryIdx].pt);
            }
            
            // Find homograph matrix
            cv::Mat homography_mat;
            homography_mat = cv::findHomography(misaligned_points,ref_points,cv::RANSAC);

            // wrap misaligned image
            cv::Mat aligned_img;
            cv::warpPerspective(misaligned_img,aligned_img,homography_mat,ref_img.size());

            // Save aligned image
            cv::imwrite("../res/aligned.jpg",aligned_img);
        }
    }
}