#include <opencv2/opencv.hpp>
#include <opencv2/tracking.hpp>

#include "track_object.h"

namespace cv_exp
{
    namespace track_object
    {
        void track(const char* video_file,int tracker_type)
        {
            cv::VideoCapture video(video_file);

            if(!video.isOpened())
            {
                std::cout << "Cannot open the video" << std::endl;
                return;
            }
            
            cv::Ptr<cv::Tracker> tracker;
            switch (tracker_type)
            {
                case 1:
                tracker = cv::TrackerCSRT::create();
                break;
                case 2:
                tracker = cv::TrackerMedianFlow::create();
                break;
                case 3:
                tracker = cv::TrackerBoosting::create();
                break;
                case 4:
                tracker = cv::TrackerMIL::create();
                break;
                case 5:
                tracker = cv::TrackerTLD::create();
                break;
                case 6:
                tracker = cv::TrackerGOTURN::create();
                break;
                case 7:
                tracker = cv::TrackerMOSSE::create();
                break;
                case 8:
                tracker = cv::TrackerKCF::create();
                break;
            }

            // Read the video frame by frame
            cv::Mat frame;
            if(!video.read(frame))
            {
                std::cout << "Empty or corrupt video" << std::endl;
            }

            // Create a bbox for area to track
            cv::Rect2d bbox = cv::selectROI(frame);
            cv::rectangle(frame,bbox,cv::Scalar(0,0,255),2,cv::LINE_8);

            cv::imshow("Track",frame);
            tracker->init(frame,bbox);

            while(video.read(frame))
            {
                if(tracker->update(frame,bbox))
                {
                    cv::rectangle(frame,bbox,cv::Scalar(0,0,255),2,1);
                }

                cv::imshow("Track",frame);

                // Exit if esc is pressed
                int k = cv::waitKey(1);
                if(k == 27)
                {
                    break;
                }
            }
        }
    }
}