#include <opencv2/opencv.hpp>
#include "align_images.h"
#include "track_object.h"
#include "facial_landmarks.h"

int main(int argc, char **argv)
{
    // Call code for facial landmark detection
    cv::Mat face_img = cv::imread("../res/face.jpeg");
    cv_exp::facial_landmarks::calculate_landmarks(face_img);

    // Call code for object traking
    cv_exp::track_object::track("../res/chaplin.mp4",1);

    // Read reference Image
    cv::Mat ref_img = cv::imread("../res/actual.jpg");
    // Read scanned image
    cv::Mat misaligned_img = cv::imread("../res/scanned.jpeg");
    cv_exp::align_images::align(ref_img,misaligned_img);
}