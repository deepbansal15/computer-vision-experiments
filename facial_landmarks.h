#ifndef __HOUGH_LINES_H__
#define __HOUGH_LINES_H__

namespace cv
{
    class Mat;
}

namespace cv_exp
{
    namespace facial_landmarks
    {
        void calculate_landmarks(cv::Mat& in_img);
    }
}

#endif