Assignment - 1 (Computer Vision)

Application : 
Image alignment
Track Object
Face landmark detection

Outputs will be saved in the res/ folder
aligned.jpg for image alignment
facial_landmarks.jpg for facial facial_landmarks

Object is tracked in realtime for track Object

Requirements:
OpenCV 3.4 

Building : 
1) Make a build folder
2) Run cmake ..
3) make
4) ./cv-exp