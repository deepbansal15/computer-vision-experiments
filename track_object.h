#ifndef __TRACK_OBJECT_H__
#define __TRACK_OBJECT_H__

namespace cv_exp
{
    namespace track_object
    {
        void track(const char* video_file,int tracker_type);
    }
}

#endif