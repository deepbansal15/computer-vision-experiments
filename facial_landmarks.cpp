#include <opencv2/opencv.hpp>
#include <opencv2/face.hpp>

#include "facial_landmarks.h"

namespace cv_exp
{
    namespace facial_landmarks
    {
        void calculate_landmarks(cv::Mat& in_img)
        {
            // Convert image to grayscale
            cv::Mat in_gray;
            cv::cvtColor(in_img,in_gray,CV_BGR2GRAY);

            // Create a haar face classifier for detecting faces
            cv::CascadeClassifier classifier("../res/haarcascade_frontalface_alt2.xml");

            // Create landmark detector
            cv::Ptr<cv::face::Facemark> face_landmark = cv::face::FacemarkLBF::create();
            face_landmark->loadModel("../res/lbfmodel.yaml");

            // Find faces
            std::vector<cv::Rect> faces;
            classifier.detectMultiScale(in_gray,faces);

            std::vector<std::vector<cv::Point2f> > landmarks;
            if(face_landmark->fit(in_gray,faces,landmarks))
            {
                for(int i=0;i<landmarks.size();i++)
                {
                    for(int j=0;j<landmarks[i].size();j++)
                    {
                        circle(in_img,landmarks[i][j],3, cv::Scalar(255,0,0), cv::FILLED);
                    }
                }
                
            }

            cv::imshow("Facial Landmarks", in_img);
            cv::imwrite("../res/facial_landmarks.jpg",in_img);
        }
    }
}